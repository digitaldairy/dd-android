package com.digitalconsultant.core.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

	public static final String IFSC_PATTERN = "^([A-Z]{4}[0-9]{7})$";

	public static final String VEHICLE_PATTERN = "^([A-Za-z]{2}[0-9]{2}[A-Za-z]{1,3}[0-9]{1,4})$";

	public static final String VEHICLE_PATTERN_FOR_SEARCH = "^([A-Z]{2}[0-9]{2}[A-Z]{1,3}[0-9]{4})$";

	public static boolean validate(final String regex, String value) {
		boolean result = false;
		try {
			if (value != null) {
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(value);
				result = matcher.matches();
			}
		} catch (Throwable throwable) {
			throwable.printStackTrace();
		}
		return result;
	}
}
