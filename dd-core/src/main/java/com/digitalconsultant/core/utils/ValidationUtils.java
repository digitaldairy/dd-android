package com.digitalconsultant.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

public class ValidationUtils {

    public static Boolean isStringNull(String toCheck) {
        if (toCheck != null && toCheck.trim().length() > 0)
            return false;
        else
            return true;
    }

    public static Boolean isNumberNull(Number n) {
        if (n != null)
            return false;
        else
            return true;
    }

    public static Boolean isNumberNullAndZero(Number n) {
        if (n != null && n.intValue() > 0)
            return false;
        else
            return true;
    }

    public static Boolean isDateNull(Date date) {

        if (date != null)
            return false;
        else
            return true;
    }

    public static boolean isListNull(Collection<?> list) {
        if (list == null || list.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static Boolean isDateafterCurrent(Date date, String format) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date Current = null;
        boolean result = false;
        try {
            Current = dateFormat.parse(dateFormat.format(new Date()));
            if (Current.after(date))
                result = true;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static boolean isArrayEmpty(Object[] array) {
        boolean result = false;
        if (array == null || array.length <= 0) {
            result = true;
        }
        return result;
    }


}
