package com.digitalconsultant.core.pojo.request;

import com.digitalconsultant.core.dto.ConfigDtls;
import com.digitalconsultant.core.dto.DairyDtls;

/**
 * Created by aniket.patil_3887 on 2/13/2017.
 */
public class RegistrationRequest extends Request {

    private DairyDtls dairyDetails;
    private ConfigDtls configDetails;

    public DairyDtls getDairyDetails() {
        return dairyDetails;
    }

    public void setDairyDetails(DairyDtls dairyDetails) {
        this.dairyDetails = dairyDetails;
    }

    public ConfigDtls getConfigDetails() {
        return configDetails;
    }

    public void setConfigDetails(ConfigDtls configDetails) {
        this.configDetails = configDetails;
    }


}
