package com.digitalconsultant.core.service;

import android.app.Activity;

import com.digitalconsultant.core.http.HttpClient;
import com.digitalconsultant.core.pojo.request.LoginRequest;
import com.digitalconsultant.core.pojo.response.LoginResponse;

/**
 * Created by aniket.patil_3887 on 2/28/2017.
 */
public interface LoginService {

    void login(LoginRequest loginRequest, HttpClient.HttpResponseListener<LoginResponse> httpResponseListener, Activity activity);
}
