package com.digitalconsultant.core.service;

import android.app.Activity;

import com.digitalconsultant.core.data.URI;
import com.digitalconsultant.core.http.HttpClient;
import com.digitalconsultant.core.pojo.request.LoginRequest;
import com.digitalconsultant.core.pojo.response.LoginResponse;

/**
 * Created by aniket.patil_3887 on 2/28/2017.
 */
public class LoginServiceImpl implements LoginService {
    @Override
    public void login(LoginRequest loginRequest, HttpClient.HttpResponseListener<LoginResponse> httpResponseListener, Activity activity) {
        try {
            HttpClient httpClient = new HttpClient(URI.LOGIN_URI, HttpClient.HttpMethod.POST, null, "");
            httpClient.responseClass(LoginResponse.class).requestBody(loginRequest)
                    .httpListener(httpResponseListener).processRequest();
        } catch (Exception e) {
            httpResponseListener.onException(e);
        }
    }
}
