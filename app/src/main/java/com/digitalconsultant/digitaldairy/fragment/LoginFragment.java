package com.digitalconsultant.digitaldairy.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.digitalconsultant.core.enumeration.Error;
import com.digitalconsultant.core.enumeration.Shift;
import com.digitalconsultant.core.http.HttpClient;
import com.digitalconsultant.core.pojo.request.LoginRequest;
import com.digitalconsultant.core.pojo.response.LoginResponse;
import com.digitalconsultant.core.service.LoginService;
import com.digitalconsultant.core.service.LoginServiceImpl;
import com.digitalconsultant.core.utils.ValidationUtils;
import com.digitalconsultant.digitaldairy.R;

import java.util.Arrays;

/**
 * Created by aniket.patil on 1/25/2017.
 */
public class LoginFragment extends Fragment implements View.OnClickListener, HttpClient.HttpResponseListener<LoginResponse> {

    private View fragmentView;
    private EditText etUserName;
    private EditText etPassword;
    private LoginService loginService;
    private AutoCompleteTextView ddShift;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_login, container, false);
        etUserName = (EditText) fragmentView.findViewById(R.id.user_name);
        etPassword = (EditText) fragmentView.findViewById(R.id.password);
        ddShift = (AutoCompleteTextView) fragmentView.findViewById(R.id.dd_shift);
        ArrayAdapter<Shift> arrayAdapter = new ArrayAdapter<Shift>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, Arrays.asList(Shift.values()));
        ddShift.setAdapter(arrayAdapter);
        Button btSignIn = (Button) fragmentView.findViewById(R.id.email_sign_in_button);
        btSignIn.setOnClickListener(this);
        loginService = new LoginServiceImpl();
        return fragmentView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.email_sign_in_button:
                process();
                break;
        }
    }

    private void process() {
        boolean error = false;
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUserName(etUserName.getText().toString());
        loginRequest.setPassword(etPassword.getText().toString());
        loginRequest.setShift(Shift.valueOf(ddShift.getText().toString()));
        if (ValidationUtils.isStringNull(loginRequest.getUserName())) {
            error = true;
            etUserName.setError(getString(R.string.error_enter_user_name));
        }

        if (ValidationUtils.isStringNull(loginRequest.getPassword())) {
            error = true;
            etPassword.setError(getString(R.string.error_enter_password));
        }
        if (!error)
            loginService.login(loginRequest, this, getActivity());
    }


    @Override
    public void onSuccess(LoginResponse response) {
        Toast.makeText(getActivity(), response.getDairyName(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onHttpError(int errorCode, String errorMessage) {

    }

    @Override
    public void onException(Exception e) {

    }

    @Override
    public void onBusinessError(Error error, LoginResponse response) {
        Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {

    }
}
